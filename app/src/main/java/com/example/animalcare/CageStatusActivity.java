package com.example.animalcare;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.animalcare.properties.BASE_URL;

public class CageStatusActivity extends AppCompatActivity {

    TextView cageStatus;
    String statusString;
    ImageView locked,unlocked;
    String clientID;
    ViewGroup viewGroup;
    public Handler handler;
    public Runnable myRunnable;
    public AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cage_status);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        cageStatus = (TextView) findViewById(R.id.cageStatus);

        locked = (ImageView) findViewById(R.id.locked);
        unlocked = (ImageView) findViewById(R.id.unlocked);

        handler = new Handler();
        final int delay = 1000; //milliseconds

        myRunnable = new Runnable(){

            @Override
            public void run() {
                getCageStatus(0);
                if (unlocked.getVisibility() == View.VISIBLE) {
                    sendNotification(viewGroup);
                }
                handler.postDelayed(this, delay);
            }
        };

        handler.postDelayed(myRunnable,delay);

//        handler.postDelayed(new Runnable(){
//            public void run(){
//                getCageStatus(0);
//                if (unlocked.getVisibility() == View.VISIBLE) {
//                    sendNotification(viewGroup);
//                }
//                handler.postDelayed(this, delay);
//            }
//        }, delay);


        AlertDialog.Builder builder =new AlertDialog.Builder(CageStatusActivity.this);
        builder.setTitle("User Not Found");
        builder.setMessage("This user is not assigned to this cage");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog = builder.create();


        Intent iin= getIntent();
        Bundle b = iin.getExtras();

        if(b!=null)
        {
            clientID =(String) b.get("EmpId");
        }

        viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0); //auto generate

        
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void getCageStatus(int id){

        AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {

                OkHttpClient client = new OkHttpClient(); //OkHttp = libary
                Request request = new Request.Builder()
                        .url(BASE_URL+"cagelocks/"+clientID)
                        .build();
                try {
                    Response response = client.newCall(request).execute(); //call to server and put response to variable

                    JSONObject object = new JSONObject(response.body().string());

                        //cageStatus data = new cageStatus(object.getInt("empID"),object.getInt("cageID"),object.getString("status"),object.getInt("_v"));

                        statusString = object.getString("Status");



                } catch (IOException e) {
                    e.printStackTrace();

                } catch (JSONException e) {
                    System.out.println("End of content");
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                cageStatus.setText(statusString);
                if(statusString != null && statusString.equalsIgnoreCase("locked")){
                    locked.setVisibility(View.VISIBLE);
                    unlocked.setVisibility(View.GONE);
                } else if (statusString != null && statusString.equalsIgnoreCase("unlocked")){
                    locked.setVisibility(View.GONE);
                    unlocked.setVisibility(View.VISIBLE);
                } else if(statusString.equalsIgnoreCase("NotFound")){
                    handler.removeCallbacks(myRunnable);
                    alertDialog.show();
                }
            }
        };

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) //android n above then this
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, id);
        else
            task.execute(id); //if not this
//        task.execute(id);
    }

    public void popupMessage(String cageStatus){

        if(cageStatus.equalsIgnoreCase("unlocked")){


        }

    }

    public void sendNotification(View view) {
            NotificationCompat.Builder mBuilder =
                    (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.ic_unlock_noti)
                            .setContentTitle("Cage door lock status")
                            .setContentText("Unlocked");

            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(001, mBuilder.build());
    }

}

