package com.example.animalcare;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.R.attr.delay;
import static com.example.animalcare.properties.BASE_URL;

public class TempMon extends AppCompatActivity {

    String tempString;
    String heaterString;
    String coolerString;
    TextView textTemp;
    TextView textHeater;
    TextView textCooler;
    String clientID; // paste here
    public AlertDialog alertDialog;
    public Runnable myRunnable;
    public Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_mon);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        textTemp = (TextView) findViewById(R.id.textTemp);
        textHeater = (TextView) findViewById(R.id.textHeater);
        textCooler = (TextView) findViewById(R.id.textCooler);

        handler = new Handler();
        final int delay = 1000; //milliseconds

        myRunnable = new Runnable() {
            @Override
            public void run() {
                getTempStatus(0);
                handler.postDelayed(this, delay);
            }
        };

        handler.postDelayed(myRunnable, delay);

        AlertDialog.Builder builder =new AlertDialog.Builder(TempMon.this);
        builder.setTitle("User Not Found");
        builder.setMessage("This user is not assigned to this cage");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog = builder.create();

// paste here
        Intent iin= getIntent();
        Bundle b = iin.getExtras();

        if(b!=null)
        {
            clientID =(String) b.get("EmpId");
        }
//
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void getTempStatus(int id){

        AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(BASE_URL+"cagetemps/"+clientID)
                        .build();
                try {
                    Response response = client.newCall(request).execute();

                    // JSONArray jsonArray = new JSONArray(response.body().string());

                    JSONObject object = new JSONObject(response.body().string());

                    //cageStatus data = new cageStatus(object.getInt("empID"),object.getInt("cageID"),object.getString("status"),object.getInt("_v"));

                    tempString = object.getString("TempValue");
                    heaterString = object.getString("Cooler");
                    coolerString = object.getString("Heater");


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                textTemp.setText(tempString);
                textHeater.setText(heaterString);
                textCooler.setText(coolerString);
                if(tempString != null && tempString.equalsIgnoreCase("NotFound")){
                    handler.removeCallbacks(myRunnable);
                    alertDialog.show();
                }

            }
        };

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, id);
        else
            task.execute(id);
//        task.execute(id);
    }

}
