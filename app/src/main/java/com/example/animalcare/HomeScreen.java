package com.example.animalcare;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class HomeScreen extends AppCompatActivity {

    Button cageButton;
    Button TempButton;
    Button PhButton;
    Button FeedButton;
    Button setValue;
    String idEditTextString;
    EditText idEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        idEditText = (EditText) findViewById(R.id.idEditText);
        idEditTextString = "";
        setValue = (Button) findViewById(R.id.setValue);
        setValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                idEditTextString = idEditText.getText().toString();
                if(idEditTextString.equalsIgnoreCase(""))
                {Toast.makeText(HomeScreen.this, "Please set the Employee ID" ,
                        Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(HomeScreen.this, "Id set to " + idEditTextString,
                            Toast.LENGTH_LONG).show();
                }
            }
        });




        TempButton = (Button) findViewById(R.id.TempMonButton);
        TempButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(idEditTextString.equalsIgnoreCase("")){
                    Toast.makeText(HomeScreen.this, "Please set the Employee ID" ,
                            Toast.LENGTH_LONG).show();
                } else {
                    Intent i = new Intent(HomeScreen.this, TempMon.class);
                    i.putExtra("EmpId", idEditTextString); //EmpId = key of bundle
                    startActivity(i);
                }
            }
        });

        PhButton = (Button) findViewById(R.id.pHMonButton);
        PhButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(idEditTextString.equalsIgnoreCase("")){
                    Toast.makeText(HomeScreen.this, "Please set the Employee ID" ,
                            Toast.LENGTH_LONG).show();
                } else {
                    Intent i = new Intent(HomeScreen.this, pHMon.class);
                    i.putExtra("EmpId", idEditTextString);
                    startActivity(i);
                }
            }
        });

        cageButton = (Button) findViewById(R.id.lockStatusButton);
        cageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(idEditTextString.equalsIgnoreCase("")){
                    Toast.makeText(HomeScreen.this, "Please set the Employee ID" ,
                            Toast.LENGTH_LONG).show();
                } else {
                    Intent i = new Intent(HomeScreen.this, CageStatusActivity.class);
                    i.putExtra("EmpId",idEditTextString);
                    startActivity(i);
                }

            }
        });


        FeedButton = (Button) findViewById(R.id.FishFeedButton);
        FeedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(idEditTextString.equalsIgnoreCase("")){
                    Toast.makeText(HomeScreen.this, "Please set the Employee ID" ,
                            Toast.LENGTH_LONG).show();
                } else {
                    Intent i = new Intent(HomeScreen.this, FishFeed.class);
                    i.putExtra("EmpId", idEditTextString);
                    startActivity(i);
                }
            }
        });





        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

