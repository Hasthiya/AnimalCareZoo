package com.example.animalcare;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.example.animalcare.R.id.tvTime;
import static com.example.animalcare.R.id.tvTime2;
import static com.example.animalcare.properties.BASE_URL;

public class FishFeed extends AppCompatActivity {

    private TextView tvDisplayTime;
    private TimePicker timePicker1;
    private Button btnChangeTime;

    private TextView tvDisplayTime2;
    private TimePicker timePicker2;
    private Button btnChangeTime2;

    private int hour;
    private int minute;

    static final int TIME_DIALOG_ID = 999; //required to identify the two timepickers
    static final int TIME_DIALOG_ID2 = 9992;

    String clientID;
    String amTime;
    String pmTime;
    String statusString;
    public AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fish_feed);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tvDisplayTime = (TextView) findViewById(tvTime);
        tvDisplayTime2 = (TextView) findViewById(tvTime2);
        setCurrentTimeOnView();
        addListenerOnButton();

        setCurrentTimeOnView2();
        addListenerOnButton2();

        Intent iin= getIntent();
        Bundle b = iin.getExtras();

        if(b!=null)
        {
            clientID =(String) b.get("EmpId");
        }

        getCageStatus(0);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        AlertDialog.Builder builder =new AlertDialog.Builder(FishFeed.this);
        builder.setTitle("User Not Found");
        builder.setMessage("This user is not assigned to this cage");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog = builder.create();
    }

    public void setCurrentTimeOnView() {

        tvDisplayTime = (TextView) findViewById(tvTime);
        timePicker1 = (TimePicker) findViewById(R.id.timePicker1);
        timePicker1.setIs24HourView(false); //didnt work

        final Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);

        // set current time into textview
        tvDisplayTime.setText(
                new StringBuilder().append(pad(hour))
                        .append(":").append(pad(minute)));



        // set current time into timepicker
        timePicker1.setCurrentHour(hour);   //old methods deplicated
        timePicker1.setCurrentMinute(minute);

    }

    public void setCurrentTimeOnView2() {

        tvDisplayTime2 = (TextView) findViewById(tvTime2);
        timePicker2 = (TimePicker) findViewById(R.id.timePicker2);
        hideAmPmLayout(timePicker2);

        final Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);

        // set current time into textview
        tvDisplayTime2.setText(
                new StringBuilder().append(pad(hour))
                        .append(":").append(pad(minute)));

        // set current time into timepicker
        timePicker2.setCurrentHour(hour);
        timePicker2.setCurrentMinute(minute);

    }

    public void addListenerOnButton() {

        btnChangeTime = (Button) findViewById(R.id.btnChangeTime);

        btnChangeTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(TIME_DIALOG_ID);

            }

        });

    }

    public void addListenerOnButton2() {

        btnChangeTime2 = (Button) findViewById(R.id.btnChangeTime2);

        btnChangeTime2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(TIME_DIALOG_ID2);

            }

        });

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:
                // set time picker as current time
                return new TimePickerDialog(this,
                        timePickerListener, hour, minute,false);
            case TIME_DIALOG_ID2:
                return new TimePickerDialog(this,
                        timePickerListener2, hour, minute,false); //case 1 : 1st picker 2: 2nd

        }
        return null;
    }

    private TimePickerDialog.OnTimeSetListener timePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int selectedHour,
                                      int selectedMinute) {
                    hour = selectedHour;
                    minute = selectedMinute;

                    // set current time into textview
                    tvDisplayTime.setText(new StringBuilder().append(pad(hour))
                            .append(":").append(pad(minute)));

                    postTime(0,tvDisplayTime.getText().toString(),"AMtime");

                    // set current time into timepicker
                    timePicker1.setCurrentHour(hour);
                    timePicker1.setCurrentMinute(minute);
                    hideAmPmLayout(timePicker1);

                }
            };

    private TimePickerDialog.OnTimeSetListener timePickerListener2 =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int selectedHour,
                                      int selectedMinute) {
                    hour = selectedHour;
                    minute = selectedMinute;

                    // set current time into textview
                    tvDisplayTime2.setText(new StringBuilder().append(pad(hour))
                            .append(":").append(pad(minute)));

                    postTime(0,tvDisplayTime2.getText().toString(),"PMtime");

                    // set current time into timepicker
                    timePicker2.setCurrentHour(hour);
                    timePicker2.setCurrentMinute(minute);

                }
            };

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    private void getCageStatus(int id){

        AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(BASE_URL+"feedingtimes/getTime/"+clientID)
                        .build();
                try {
                    Response response = client.newCall(request).execute();


                    JSONObject object = new JSONObject(response.body().string());

                    //cageStatus data = new cageStatus(object.getInt("empID"),object.getInt("cageID"),object.getString("status"),object.getInt("_v"));

                    amTime = object.getString("AMtime");
                    pmTime = object.getString("PMtime");


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                tvDisplayTime.setText(amTime);
                tvDisplayTime2.setText(pmTime);
                if(amTime != null && amTime.equalsIgnoreCase("NotFound")){
                    alertDialog.show();
                } else if (amTime != null && amTime.equalsIgnoreCase("NotFound")) {
                    alertDialog.show();
                }

            }
        };

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, id);
        else
            task.execute(id);
//        task.execute(id);
    }

    private void postTime(int id, final String time, final String AMPM){

        AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {

//                OkHttpClient client = new OkHttpClient();
//                RequestBody requestBody = new MultipartBody.Builder()
//                        .setType(MultipartBody.FORM)
//                        .addFormDataPart("AMtime", "03.33")
//                        .build();
//                Request request = new Request.Builder()
//                        .url(BASE_URL+"feedingtimes/"+clientID)
//                        .method("PUT", RequestBody.create(null, new byte[0]))
//                        .post(requestBody)
//                        .build();

                String jsonBody = "{\"" + AMPM + "\" : \" " + time + "\"}";

                MediaType JSON = MediaType.parse("application/json; charset=utf-8"); //put req headers
                RequestBody body = RequestBody.create(JSON, jsonBody);

                OkHttpClient client = new OkHttpClient();

                Request request = new Request.Builder()
                        .url(BASE_URL+"feedingtimes/"+clientID)
                        .put(body) //PUT
                        .build();

                try {
                    Response response = client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                return null;
            }

        };

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, id);
        else
            task.execute(id);
//        task.execute(id);
    }

    private void hideAmPmLayout(TimePicker picker) {
        final int id = Resources.getSystem().getIdentifier("ampm_layout", "id", "android");
        final View amPmLayout = picker.findViewById(id);
        if(amPmLayout != null) {
            amPmLayout.setVisibility(View.GONE);
        }
    }

}
